import React from 'react';
import styled from 'styled-components';

export const Button = ({text, backgroundColor,onClick}) => {
    return (
        <div>
           <StyledButton 
            style={{backgroundColor}}
            onClick={onClick}
           >{text}
           </StyledButton>
        </div>
    )
}

const StyledButton = styled.button`
    width: 80px;
    height: 35px;
    border: 2px solid transparent;
    border-radius: 6px;
    font-size: 13.2px;
    line-height: 18px;
    color: #ffffff;
    font-family: "Helvetica";
    cursor: pointer;
    border: none;
    outline: none;
    margin-left: 10px;
`

export default Button;