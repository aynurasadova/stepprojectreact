import React from 'react';
import styled from 'styled-components';
export const Modal = ({actions, text, backgroundColor}) => {
    
    return(
    <StyledModal
    style={{backgroundColor}}>

        <MainModal>
            <p className = "modal-text">{text}</p>
            <Button>
                {actions}
            </Button>
        </MainModal>
        
    </StyledModal>
    )
}

const StyledModal = styled.div`
    margin: 0;
    padding: 0;
    width: 30%;
    height: 40vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    border: 2px solid transparent;
    border-radius: 10px;
    font-family: "Helvetica";
    color: rgba(0,0,0, 0.7);
    //Centering

    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */

    top: 0%;
    left: 34%;  
    padding: 20px;
    text-align: center;
    position: fixed;
    filter: blur(0);
    z-index: 1;
`

const MainModal = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`

const ModalText = styled.p`
    margin-left: 12px;
    padding: 5px;
    width: 85%;
    height: 70px;
    line-height: 30px;
    font-size: 21px;
    text-align: center;
`

const Button = styled.div`
    display: flex;
    margin-bottom: 10px;   
    outline: none;
    border: none;
`