import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Header} from './AzizaAkhmedova';
import {NoteContextProvider} from './context/notes';
import {Homepage, Create, Edit, Actual, Archive, SingleNotePage} from './pages';


function App() {
  return (
    <NoteContextProvider>
      <Router>
        <Header />
        <Switch>
          <Route path = "/" exact component = {Homepage} />
          <Route path = "/create" exact component = {Create} />
          <Route path="/notes/:id" exact component={SingleNotePage} />
          <Route path = "/edit/:id" exact component = {Edit}/>

          <Route path = "/actualNotes" exact component = {Actual}/>
          <Route path = "/archivedNotes" exact component = {Archive}/>
        </Switch>
      </Router>
    </NoteContextProvider>
    
  );
}

export default App;


