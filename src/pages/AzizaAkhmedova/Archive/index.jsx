import React, {useContext, useState, useEffect} from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import { Note } from '../../../AynurAsadova';
import { NoteContext } from '../../../context/notes';
import { Row, Container, Title } from '../../../AzizaAkhmedova';

export const Archive = () => {

    const  {notes} = useContext(NoteContext);

    const [items, setItems] = useState([]);

    useEffect(() => {
        setItems(notes.filter(note => note.status === true))
    },[notes])

    return(
            <Container>
                <Title>Completed Notes</Title>
                <Row>
                    {
                        items && items.map(item => (
                            <StyledNav to = {`/notes/${item.id}`} key = {item.id} >
                                <Note key = {item.id} note= {item}/>
                            </StyledNav>
                            ))
                    }
                </Row>
            </Container>
    )
}


const StyledNav = styled(NavLink)`
    text-decoration: none;
`;
