import React, {useContext, useState} from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import { Button, Modal } from '../../../AzizaAkhmedova';
import { NoteContext } from '../../../context/notes';



const domain = 'http://localhost:3001/notes/'

export const SingleNotePage = ({initial = {}, history, match: {params: {id}}}) => {

    const {notes, deleteNote} = useContext(NoteContext);
    
    const note = notes.find(item => item.id == id);

    //DELETE

    const [modalStatus, setmodalStatus] = useState(false);    
    
    const deleteMethod = {
        method: 'DELETE', // Method itself
        headers: {
         'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
        },
        body: JSON.stringify(notes) // We send data in JSON format
    }
    
    const deletedNote = async(id) => {
        deleteNote(id);
        fetch(`${domain}/${note.id}`, deleteMethod);
        history.push("/");
    };
    
    const toggleModal = () => {
        setmodalStatus(v => !v)
    }

    const [edit, setEdit] = useState(false);
    
    //ARCHIVE
    
    const archive = {
        ...note,
        ...initial
    };

    const onArchive = async () => {
            await fetch(`${domain}/${archive.id}`, {
                method: "PATCH",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    ...archive, 
                    status: !archive.status
                })
            })
            history.goBack();
    }


    return(
        <Container>
            {
                note && (
                    <Popup>
                        <Close onClick={() => history.goBack()}>x</Close>
                        <Context1 bg = {note.color}>
                            <span>
                                {note.title}
                            </span>
                                <br />
                                {note.text}
                        </Context1>
                        <Context2>
                            <NavLink 
                                    style = {{textDecoration: "none"}} 
                                    to = {`/edit/${note.id}`}>
                                <StyledButton onClick = {() => {
                                    setEdit(true);
                                }}>{edit ? "save": "edit"}
                                </StyledButton>
                            </NavLink>

                            <StyledButton onClick = {onArchive}
                            >{note.status ? "actual" : "archive"}</StyledButton>
                            

                            <div>
                                <StyledButton onClick = {toggleModal}
                                >delete
                                </StyledButton >

                                {(modalStatus && (
                                    <Modal
                                        backgroundColor = "#e6e6e6"
                                        text = "Do you want to delete this note?"
                                        close = {toggleModal}
                                        actions = {[
                                            <Button 
                                                key = {1}
                                                backgroundColor='rgba(0,0,0,.1)' 
                                                text='Delete'
                                                onClick = {() => {
                                                    deletedNote(note.id);
                                                    toggleModal();
                                                    history.push('/');
                                                }}/>,
                                            <Button 
                                                key ={2}
                                                backgroundColor='rgba(0,0,0,.1)' 
                                                text='Cancel' 
                                                onClick={toggleModal}/>]} />
                                ))} 
                            </div>
                            

                            
                        </Context2>
                    </Popup>
                )
            }
        </Container>
    )
};

const Container = styled.div`
    position: fixed;
    background: rgba(0,0,0,.3);
    display: flex;
    justify-content: center;
    align-items: center;
    color: white;
    z-index: 1000;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
`

const Popup = styled.div`
    width: 55%;
    padding: 40px;
    text-align:center;
    max-width: 600px;
    color: white;
    border-radius: 15px;
    background-color: rgba(124, 76, 116, .9);
    height: 50%;
    position: relative;
    display: flex;
    justify-content: center;
`

const Context1 = styled.div`
    NavLink {
        text-decoration: none;
    }
    border: 1px solid grey;
    border-radius: 15px;
    span{
        line-height: 50px;
        font-size: 30px;
        border-bottom: 2px solid white;
    }
    font-size: 20px;
    width: 96%;
    height: auto;
    background-color: ${p => p.bg};
`

const Context2 = styled.div`
`

const StyledButton = styled.button`
    outline: none;
    text-transform: uppercase;
    color: white;
    background-color: rgb(112, 26, 91);
    border-radius: 6px;
    height: 32px;
    width: 90px;
    margin: 10px;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 14px;
    cursor: pointer;
    border: none;
    box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.35);
`

const Close = styled.button`
    display: block;
    width: 30px;
    height: 30px;
    border-radius: 100%;
    background: rgba(0,0,0,.6);
    color: white;
    border: none;
    position: absolute;
    top: 10px;
    right: 10px;
    cursor: pointer;
    outline: none;
    font-size: 18px;
`