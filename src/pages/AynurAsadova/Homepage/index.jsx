import React from 'react';
import { NoteList } from './components';
import { Container, Title } from '../../../AzizaAkhmedova/commons';


export const Homepage = () => {

    return(
        <Container>
            <Title>Happy Notes</Title>
            <NoteList />
        </Container>
    )
};

